## Ansible role for installing passlib (if not yet).

Some [ansible modules](http://docs.ansible.com/ansible/latest/htpasswd_module.html) and [filters](http://docs.ansible.com/ansible/latest/playbooks_filters.html#hashing-filters) require, that `passlib` has been installed on machine, where role will be played. So, this role solves this problem.
